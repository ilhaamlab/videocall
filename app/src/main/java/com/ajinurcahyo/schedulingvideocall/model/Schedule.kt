package com.ajinurcahyo.schedulingvideocall.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Schedule (
    val id: Long,
    val date: String,
    val time: String
) : Parcelable
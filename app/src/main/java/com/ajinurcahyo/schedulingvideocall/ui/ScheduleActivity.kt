package com.ajinurcahyo.schedulingvideocall.ui

import android.app.AlarmManager
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.ajinurcahyo.schedulingvideocall.AlarmReceiver
import com.ajinurcahyo.schedulingvideocall.DatabaseHelper
import com.ajinurcahyo.schedulingvideocall.R
import com.ajinurcahyo.schedulingvideocall.adapter.ListScheduleAdapter
import com.ajinurcahyo.schedulingvideocall.databinding.ActivityScheduleBinding
import com.ajinurcahyo.schedulingvideocall.model.Schedule
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

class ScheduleActivity : AppCompatActivity(), ListScheduleAdapter.OnItemClickListener {

    private lateinit var binding: ActivityScheduleBinding
    private val handler = Handler(Looper.getMainLooper())
    private lateinit var selectedDate: String
    private lateinit var selectedTime: String
    private lateinit var dbHelper: DatabaseHelper

    private lateinit var alarmReceiver: AlarmReceiver

    private var isEditing = false
    private var editingItemId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityScheduleBinding.inflate(layoutInflater)
        setContentView(binding.root)
        adjustInsets()
        initialize()
        setupListeners()
    }

    private fun adjustInsets() {
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { view, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            view.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    // inisialisasi awal aktivitas
    private fun initialize() {
        startRealTimeUpdate()
        dbHelper = DatabaseHelper(this)
        dbHelper.deletePastDateTime()
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        displayData()
    }

    private fun setupListeners() {
        binding.btnDate.setOnClickListener {
            showDatePickerDialog()
        }

        binding.btnTime.setOnClickListener {
            showTimePickerDialog()
        }

        binding.btnConfirm.setOnClickListener {
            if (isEditing) {
                updateDateTimeInDatabase()
            } else {
                saveDateTimeToDatabase()
                setAlarm(1, selectedDate, selectedTime)
            }
            displayData()
            clearSelection()
        }

        binding.btnBack.setOnClickListener {
            finish()
        }
    }

    private fun clearSelection() {
        binding.btnDate.text = getString(R.string.pilih_tanggal)
        binding.btnTime.text = getString(R.string.pilih_waktu)
        selectedDate = ""
        selectedTime = ""
    }

    private fun startRealTimeUpdate() {
        val runnable = object : Runnable {
            override fun run() {
                updateDateTime()
                handler.postDelayed(this, 1000)
            }
        }
        handler.post(runnable)
    }

    private fun updateDateTime() {
        binding.txtDateTime.text = getCurrentDateTime()
    }

    private fun getCurrentDateTime(): String {
        val dateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
        return dateFormat.format(Date())
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacksAndMessages(null)
    }

    private fun showDatePickerDialog() {
        val calendar = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(this, { _, year, month, day ->
            selectedDate = String.format(Locale.getDefault(), "%02d/%02d/%d", day, month + 1, year)
            binding.btnDate.text = selectedDate
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        datePickerDialog.show()
    }

    private fun showTimePickerDialog() {
        val calendar = Calendar.getInstance()
        val timePickerDialog = TimePickerDialog(this, { _, hour, minute ->
            selectedTime = String.format(Locale.getDefault(), "%02d:%02d", hour, minute)
            binding.btnTime.text = selectedTime
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true)
        timePickerDialog.show()
    }

    private fun saveDateTimeToDatabase() {
        if (selectedDate.isNotBlank() && selectedTime.isNotBlank()) {
            val id = dbHelper.insertDateTime(selectedDate, selectedTime)
            if (id > 0) {
                showToast(R.string.data_saved)
            } else {
                showToast(R.string.data_save_failed)
            }
        } else {
            showToast(R.string.select_date_time_first)
        }
    }

    private fun displayData() {
        val dateTimeList = dbHelper.getAllDateTime()
        val adapter = ListScheduleAdapter(dateTimeList, this)
        binding.recyclerView.adapter = adapter
    }

    override fun onEditClick(dateTime: Schedule, position: Int) {
        showEditConfirmationDialog(dateTime)
    }

    private fun updateDateTimeInDatabase() {
        if (selectedDate.isNotBlank() && selectedTime.isNotBlank()) {
            val rowsAffected = dbHelper.updateDateTime(editingItemId, selectedDate, selectedTime)
            if (rowsAffected > 0) {
                showToast(R.string.data_updated_successfully)
            } else {
                showToast(R.string.failed_to_update_data)
            }
            resetEditingState()
        } else {
            showToast(R.string.please_select_date_time_first)
        }
    }

    private fun showEditConfirmationDialog(dateTime: Schedule) {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.edit_confirmation_message))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.yes)) { dialog, _ ->
                enterEditingState(dateTime)
                dialog.dismiss()
            }
            .setNegativeButton(getString(R.string.no)) { dialog, _ ->
                dialog.dismiss()
            }
            .setTitle(getString(R.string.edit_confirmation_title))
            .create()
            .show()
    }

    private fun enterEditingState(dateTime: Schedule) {
        isEditing = true
        editingItemId = dateTime.id
        binding.btnConfirm.text = getString(R.string.update)
        selectedDate = dateTime.date
        selectedTime = dateTime.time
        binding.btnDate.text = selectedDate
        binding.btnTime.text = selectedTime
    }

    private fun resetEditingState() {
        isEditing = false
        binding.btnConfirm.text = getString(R.string.confirm)
    }

    private fun showToast(messageResId: Int) {
        Toast.makeText(this, getString(messageResId), Toast.LENGTH_SHORT).show()
    }

    private fun setAlarm(id: Int, date: String, time: String) {
        alarmReceiver = AlarmReceiver()
        alarmReceiver.setOneTimeAlarm(this, AlarmReceiver.TYPE_ONE_TIME, date, time, "You will have video call session at $time")
    }

    private fun cancelAlarm(id: Int) {
        //  logika pembatalan alarm
    }
}
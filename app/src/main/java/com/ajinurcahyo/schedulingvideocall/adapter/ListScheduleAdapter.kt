package com.ajinurcahyo.schedulingvideocall.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ajinurcahyo.schedulingvideocall.R
import com.ajinurcahyo.schedulingvideocall.model.Schedule

class ListScheduleAdapter(private val listSchedule: List<Schedule>, private val itemClickListener: OnItemClickListener) : RecyclerView.Adapter<ListScheduleAdapter.ListViewHolder>() {
    interface OnItemClickListener {
        fun onEditClick(dateTime: Schedule, position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_schedule, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int = listSchedule.size

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val currentItem = listSchedule[position]
        holder.tvDate.text = currentItem.date
        holder.tvTime.text = currentItem.time
        holder.btnEdit.setOnClickListener {
            itemClickListener.onEditClick(currentItem, position)
        }
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvDate: TextView = itemView.findViewById(R.id.txt_date)
        val tvTime: TextView = itemView.findViewById(R.id.txt_time)
        val btnEdit: ImageView = itemView.findViewById(R.id.btn_edit)
    }
}
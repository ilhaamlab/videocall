package com.ajinurcahyo.schedulingvideocall

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.ajinurcahyo.schedulingvideocall.model.Schedule
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "scheduling.db"
        private const val DATABASE_VERSION = 1
        const val TABLE_NAME = "datetime"
        const val COLUMN_ID = "id"
        const val COLUMN_DATE = "date"
        const val COLUMN_TIME = "time"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "CREATE TABLE $TABLE_NAME (" +
                "$COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$COLUMN_DATE TEXT, " +
                "$COLUMN_TIME TEXT)"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun insertDateTime(date: String, time: String): Long {
        val db = this.writableDatabase
        val contentValues = ContentValues().apply {
            put(COLUMN_DATE, date)
            put(COLUMN_TIME, time)
        }
        return db.insert(TABLE_NAME, null, contentValues)
    }

    fun getAllDateTime(): List<Schedule> {
        val dateTimeList = mutableListOf<Schedule>()
        val db = this.readableDatabase
        val cursor: Cursor = db.rawQuery("SELECT * FROM $TABLE_NAME", null)
        if (cursor.moveToFirst()) {
            do {
                val id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID))
                val date = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DATE))
                val time = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TIME))
                dateTimeList.add(Schedule(id, date, time))
            } while (cursor.moveToNext())
        }
        cursor.close()
        return dateTimeList
    }

    fun deletePastDateTime() {
        val db = this.writableDatabase
        val currentDate = Calendar.getInstance().time
        val dateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault())
        val cursor = db.rawQuery("SELECT * FROM $TABLE_NAME", null)
        if (cursor.moveToFirst()) {
            do {
                val dateStr = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DATE))
                val timeStr = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TIME))
                val dateTimeStr = "$dateStr $timeStr"
                val dateTime = dateFormat.parse(dateTimeStr)
                if (dateTime != null && dateTime < currentDate) {
                    val id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID))
                    db.delete(TABLE_NAME, "$COLUMN_ID=?", arrayOf(id.toString()))
                }
            } while (cursor.moveToNext())
        }
        cursor.close()
    }

    fun updateDateTime(id: Long, date: String, time: String): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues().apply {
            put(COLUMN_DATE, date)
            put(COLUMN_TIME, time)
        }
        return db.update(TABLE_NAME, contentValues, "$COLUMN_ID=?", arrayOf(id.toString()))
    }
}
